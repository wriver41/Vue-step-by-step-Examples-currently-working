Vue.component('coupon',{
	model: {
		prop: 'code',
		event: 'input'
		},
	props:['code'],
	template:`
		<input type="text" :value="code" @input="updateCode($event.target.value)">
	`,
	data(){
		return {
			invalids:['ALLFREE','SOMETHINGELSE']
		}
	},
	methods:{
		updateCode(code){
			if (this.invalids.includes(code)){
			//if (code === 'ALLFREE'){
				alert('This coupon is no longer valid. Sorry!');
				this.code ='';
				return;
			}
			this.$emit('input', code);
		}
	}
});
new Vue({
	el:'#app',
	data:{
		coupon:'Freebie',
	} 
});
